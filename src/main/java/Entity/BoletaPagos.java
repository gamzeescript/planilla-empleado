package Entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "boleta_pago")
public class BoletaPagos implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_boleta_pago")
    private int idBoletapago;
    
    @Column(name="descuento_iss")
    private Double descuentoIss;
    
    @Column(name="descuento_afp")
    private Double descuentoAfp;
    
    @Column(name="descuento_ahorro_empresa")
    private Double descuentoAhorroempresa;
    
    @Column(name="descuento_renta")
    private Double descuentoRenta;
    
    @Column(name="total_descuento")
    private Double totalDescuento;
    
    @Column(name="total_pagar")
    private Double totalPagar;
    
    @JoinColumn(name = "id_control", referencedColumnName = "id_control")
    @ManyToOne
    private ControlAsistencias controlAsistencia;

    public BoletaPagos() {
    }

    public BoletaPagos(int idBoletapago) {
        this.idBoletapago = idBoletapago;
    }

    public int getIdBoletapago() {
        return idBoletapago;
    }

    public void setIdBoletapago(int idBoletapago) {
        this.idBoletapago = idBoletapago;
    }

    public Double getDescuentoIss() {
        return descuentoIss;
    }

    public void setDescuentoIss(Double descuentoIss) {
        this.descuentoIss = descuentoIss;
    }

    public Double getDescuentoAfp() {
        return descuentoAfp;
    }

    public void setDescuentoAfp(Double descuentoAfp) {
        this.descuentoAfp = descuentoAfp;
    }

    public Double getDescuentoAhorroempresa() {
        return descuentoAhorroempresa;
    }

    public void setDescuentoAhorroempresa(Double descuentoAhorroempresa) {
        this.descuentoAhorroempresa = descuentoAhorroempresa;
    }

    public Double getDescuentoRenta() {
        return descuentoRenta;
    }

    public void setDescuentoRenta(Double descuentoRenta) {
        this.descuentoRenta = descuentoRenta;
    }

    public Double getTotalDescuento() {
        return totalDescuento;
    }

    public void setTotalDescuento(Double totalDescuento) {
        this.totalDescuento = totalDescuento;
    }

    public Double getTotalPagar() {
        return totalPagar;
    }

    public void setTotalPagar(Double totalPagar) {
        this.totalPagar = totalPagar;
    }

    public ControlAsistencias getControlAsistencia() {
        return controlAsistencia;
    }

    public void setControlAsistencia(ControlAsistencias controlAsistencia) {
        this.controlAsistencia = controlAsistencia;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 31 * hash + this.idBoletapago;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final BoletaPagos other = (BoletaPagos) obj;
        if (this.idBoletapago != other.idBoletapago) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "BoletaPagos{" + "idBoletapago=" + idBoletapago + '}';
    }
    
}
