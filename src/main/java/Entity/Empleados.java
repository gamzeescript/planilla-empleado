package Entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table
public class Empleados implements Serializable{
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id_empleado")
    private int idEmpleado;
    
    @Column(name="dui")
    private String dui;
    
    @Column(name="nit")
    private String nit;
    
    @Column(name="nombres")
    private String nombres;
    
    @Column(name="apellidos")
    private String apellidos;
    
    @Column(name="genero")
    private String genero;
    
    @Column(name="direccion")
    private String direccion;
    
    @Column(name="telefono")
    private String telefono;
    
    @Column(name="fecha_nacimiento")
    @Temporal(TemporalType.DATE)
    private Date fechaNacimiento;
    
    @Column(name="correo")
    private String correo;
    
    @Column(name="fecha")
    @Temporal(TemporalType.DATE)
    private Date fecha;
    
    @Column(name="estado")
    private int estado;
    
    /* @ManyToOne relaciones*/
    
    @ManyToOne
    @JoinColumn(name="id_departamento", referencedColumnName = "id_departamento")
    private Empleados empleado;
    
    @ManyToOne
    @JoinColumn(name="id_cargo", referencedColumnName = "id_cargo")
    private Cargos cargo;
    
    @ManyToOne
    @JoinColumn(name="id_horario", referencedColumnName = "id_horario")
    private Horarios horario;
    
    /* @OneToMany relaciones */
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "id_permiso")
    private List<Permisos> listapermiso;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "id_usuario")
    private List<Usuarios> listausuario;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "id_control")
    private List<ControlAsistencias> listaControlasistencia;

    public Empleados() {
    }

    public Empleados(int idEmpleado) {
        this.idEmpleado = idEmpleado;
    }

    public int getIdEmpleado() {
        return idEmpleado;
    }

    public void setIdEmpleado(int idEmpleado) {
        this.idEmpleado = idEmpleado;
    }

    public String getDui() {
        return dui;
    }

    public void setDui(String dui) {
        this.dui = dui;
    }

    public String getNit() {
        return nit;
    }

    public void setNit(String nit) {
        this.nit = nit;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

    public Empleados getEmpleado() {
        return empleado;
    }

    public void setEmpleado(Empleados empleado) {
        this.empleado = empleado;
    }

    public Cargos getCargo() {
        return cargo;
    }

    public void setCargo(Cargos cargo) {
        this.cargo = cargo;
    }

    public Horarios getHorario() {
        return horario;
    }

    public void setHorario(Horarios horario) {
        this.horario = horario;
    }

    public List<Permisos> getListapermiso() {
        return listapermiso;
    }

    public void setListapermiso(List<Permisos> listapermiso) {
        this.listapermiso = listapermiso;
    }

    public List<Usuarios> getListausuario() {
        return listausuario;
    }

    public void setListausuario(List<Usuarios> listausuario) {
        this.listausuario = listausuario;
    }

    public List<ControlAsistencias> getListaControlasistencia() {
        return listaControlasistencia;
    }

    public void setListaControlasistencia(List<ControlAsistencias> listaControlasistencia) {
        this.listaControlasistencia = listaControlasistencia;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 43 * hash + this.idEmpleado;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Empleados other = (Empleados) obj;
        if (this.idEmpleado != other.idEmpleado) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Empleados{" + "idEmpleado=" + idEmpleado + '}';
    }    
    
}
