package Entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="cargos")
public class Cargos implements Serializable{
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id_cargo")
    private int idCargo;
    
    @Column(name="nombre")
    private String nombre;
    
    @Column(name="salario")
    private Double salario;
    
    @Column(name="valor_hora")
    private long valorHora;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "id_departamento")
    private List<Empleados> listaEmpleadosDep;

    public Cargos() {
    }

    public Cargos(int idCargo) {
        this.idCargo = idCargo;
    }

    public int getIdCargo() {
        return idCargo;
    }

    public void setIdCargo(int idCargo) {
        this.idCargo = idCargo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Double getSalario() {
        return salario;
    }

    public void setSalario(Double salario) {
        this.salario = salario;
    }

    public long getValorHora() {
        return valorHora;
    }

    public void setValorHora(long valorHora) {
        this.valorHora = valorHora;
    }

    public List<Empleados> getListaEmpleadosDep() {
        return listaEmpleadosDep;
    }

    public void setListaEmpleadosDep(List<Empleados> listaEmpleadosDep) {
        this.listaEmpleadosDep = listaEmpleadosDep;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + this.idCargo;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Cargos other = (Cargos) obj;
        if (this.idCargo != other.idCargo) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Cargos{" + "idCargo=" + idCargo + '}';
    }
    
    
    
}
