package Entity;

import java.io.Serializable;
import java.sql.Time;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="horarios")
public class Horarios implements Serializable{
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id_horario")
    private int idHorario;
    
    @Column(name="dias")
    private String dias;
    
    @Column(name="fecha_inicial")
    @Temporal(TemporalType.DATE)
    private Date fechaInicial;
    
    @Column(name="fecha_final")
    @Temporal(TemporalType.DATE)
    private Date fechaFinal;
    
    @Column(name="hora_entrada")
    @Temporal(TemporalType.TIME)
    private Time hora_entrada;
    
    @Column(name="hora_Salida")
    @Temporal(TemporalType.TIME)
    private Time hora_Salida;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "id_empleado")
    private List<Empleados> listaEmpleadoHo;

    public Horarios() {
    }

    public Horarios(int idHorario) {
        this.idHorario = idHorario;
    }

    public int getIdHorario() {
        return idHorario;
    }

    public void setIdHorario(int idHorario) {
        this.idHorario = idHorario;
    }

    public String getDias() {
        return dias;
    }

    public void setDias(String dias) {
        this.dias = dias;
    }

    public Date getFechaInicial() {
        return fechaInicial;
    }

    public void setFechaInicial(Date fechaInicial) {
        this.fechaInicial = fechaInicial;
    }

    public Date getFechaFinal() {
        return fechaFinal;
    }

    public void setFechaFinal(Date fechaFinal) {
        this.fechaFinal = fechaFinal;
    }

    public Time getHora_entrada() {
        return hora_entrada;
    }

    public void setHora_entrada(Time hora_entrada) {
        this.hora_entrada = hora_entrada;
    }

    public Time getHora_Salida() {
        return hora_Salida;
    }

    public void setHora_Salida(Time hora_Salida) {
        this.hora_Salida = hora_Salida;
    }

    public List<Empleados> getListaEmpleadoHo() {
        return listaEmpleadoHo;
    }

    public void setListaEmpleadoHo(List<Empleados> listaEmpleadoHo) {
        this.listaEmpleadoHo = listaEmpleadoHo;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + this.idHorario;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Horarios other = (Horarios) obj;
        if (this.idHorario != other.idHorario) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Horarios{" + "idHorario=" + idHorario + '}';
    }
    
    
    
}
