package Entity;

import java.io.Serializable;
import java.sql.Time;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "control_asistencia")
public class ControlAsistencias implements Serializable {
    
    /*
    
    
    */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_control")
    private int idControl;
    
    @Column(name="fecha")
    @Temporal(TemporalType.DATE)
    private Date fecha;
    
    @Column(name="entrada")
    @Temporal(TemporalType.DATE)
    private Date entrada;
    
    @Column(name="salida")
    @Temporal(TemporalType.DATE)
    private Date salida;
     
    @Column(name="horas_trabajadas")
    @Temporal(TemporalType.TIME)
    private Time horasTrabajadas;
    
    @Column(name="nota")
    private String nota;
     
    @JoinColumn(name="id_empleado", referencedColumnName = "id_empleado")
    @ManyToOne
    private Empleados empleado;
     
    @JoinColumn(name="id_permiso", referencedColumnName = "id_permiso")
    @ManyToOne
    private Permisos permiso;
     
    @JoinColumn(name="id_hora_extra", referencedColumnName = "id_hora_extra")
    @ManyToOne
    private HorasExtras horaExtra;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "id_boleta_pago")
    private List<BoletaPagos> listaBoletapago;

    public ControlAsistencias() {
    }

    public ControlAsistencias(int idControl) {
        this.idControl = idControl;
    }

    public int getIdControl() {
        return idControl;
    }

    public void setIdControl(int idControl) {
        this.idControl = idControl;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Date getEntrada() {
        return entrada;
    }

    public void setEntrada(Date entrada) {
        this.entrada = entrada;
    }

    public Date getSalida() {
        return salida;
    }

    public void setSalida(Date salida) {
        this.salida = salida;
    }

    public Time getHorasTrabajadas() {
        return horasTrabajadas;
    }

    public void setHorasTrabajadas(Time horasTrabajadas) {
        this.horasTrabajadas = horasTrabajadas;
    }

    public String getNota() {
        return nota;
    }

    public void setNota(String nota) {
        this.nota = nota;
    }

    public Empleados getEmpleado() {
        return empleado;
    }

    public void setEmpleado(Empleados empleado) {
        this.empleado = empleado;
    }

    public Permisos getPermiso() {
        return permiso;
    }

    public void setPermiso(Permisos permiso) {
        this.permiso = permiso;
    }

    public HorasExtras getHoraExtra() {
        return horaExtra;
    }

    public void setHoraExtra(HorasExtras horaExtra) {
        this.horaExtra = horaExtra;
    }

    public List<BoletaPagos> getListaBoletapago() {
        return listaBoletapago;
    }

    public void setListaBoletapago(List<BoletaPagos> listaBoletapago) {
        this.listaBoletapago = listaBoletapago;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + this.idControl;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ControlAsistencias other = (ControlAsistencias) obj;
        if (this.idControl != other.idControl) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ControlAsistencias{" + "idControl=" + idControl + '}';
    }

}
