package Entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "horas_extras")
public class HorasExtras implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_hora_extra")
    private int idHoraextra;
    
    @Column(name="tipo_horas_extras")
    private String tipoHorasextra;
    
    @Column(name="cantidad")
    private int cantidad;
    
    @Column(name="fechas")
    @Temporal(TemporalType.DATE)
    private Date fechas;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "id_control")
    private List<ControlAsistencias> listaControlasistenciash;

    public HorasExtras() {
    }

    public HorasExtras(int idHoraextra) {
        this.idHoraextra = idHoraextra;
    }

    public int getIdHoraextra() {
        return idHoraextra;
    }

    public void setIdHoraextra(int idHoraextra) {
        this.idHoraextra = idHoraextra;
    }

    public String getTipoHorasextra() {
        return tipoHorasextra;
    }

    public void setTipoHorasextra(String tipoHorasextra) {
        this.tipoHorasextra = tipoHorasextra;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public Date getFechas() {
        return fechas;
    }

    public void setFechas(Date fechas) {
        this.fechas = fechas;
    }

    public List<ControlAsistencias> getListaControlasistenciash() {
        return listaControlasistenciash;
    }

    public void setListaControlasistenciash(List<ControlAsistencias> listaControlasistenciash) {
        this.listaControlasistenciash = listaControlasistenciash;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + this.idHoraextra;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final HorasExtras other = (HorasExtras) obj;
        if (this.idHoraextra != other.idHoraextra) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "HorasExtras{" + "idHoraextra=" + idHoraextra + '}';
    }
    
    
    
    

}
